#!/usr/bin/env python3

from sutom import Sutom
import re
import random
import argparse

class Solver:

    def __init__(self, word_length=None):
        self.game = Sutom(word_length=word_length)
        with open("words.txt", "r") as f:
            self.possible_words = [w.replace("\n", "") for w in f]
            if word_length:
                self.possible_words = list(
                    filter(
                        lambda w: len(w) == word_length, self.possible_words
                    )
                )
        self.tried_words = []

    def update_infos(self):
        self.partial_word = self.game.get_partial_word
        self.word_length = len(self.partial_word)
        self.chars_in_word = self.game.chars_in_word
        self.chars_not_in_word = self.game.chars_not_in_word

    def get_regex(self):
        """Returns a regex given the partial word, and letters we know
        they're in or not in the word.
        """
        regex = self.partial_word.replace('*', '.')
        replace_regex = ""
        positive_assert = ""
        for c in self.chars_in_word:
            positive_assert += f"(?=.*{c})"
        negative_assert = ""
        if self.chars_not_in_word:
            negative_assert = (
                f"(?!.*[{''.join(c for c in self.chars_not_in_word)}])"
            )
        regex = f"^{positive_assert}{negative_assert}{regex}$"
        return re.compile(regex)

    def update_available_words(self):
        regex = self.get_regex()
        self.possible_words = [
            w for w in self.possible_words if (
                len(w) == self.word_length
                and regex.match(w)
                and w not in self.tried_words
            )
        ]

    def get_word(self):
        return self.possible_words[0]

    def run(self):
        self.update_infos()
        print(self.partial_word)
        while "game is running":
            self.update_available_words()
            word = self.get_word()
            self.tried_words.append(word)
            won, res = self.game.turn(word)
            if won:
                print(res)
                print("WON !!!")
                exit(0)
            elif not won and not res:
                print(f"Lost… the right word was {self.game.word_to_guess}")
                exit(0)
            print(res) 
            self.update_infos()


if __name__ == "__main__":
    solver = Solver()
    solver.run()
