#!/usr/bin/env python3

import random

NUMBER_OF_TRIES = 5

class Sutom:

    def __init__(self, word_length=None):
        self.word_to_guess = self.pick_random_word(word_length=word_length)
        # The list of chars correctly guessed, on the right position
        self.known_chars = [False]*len(self.word_to_guess)
        self.known_chars[0] = self.word_to_guess[0]
        # The list of chars found, but the position is unknown
        self.chars_in_word = set()
        # The list of chars we know aren't in the word
        self.chars_not_in_word = set()
        self.turn_nr = 0

    def pick_random_word(self, word_length=None):
        with open("words.txt", "r") as f:
            words = list(f)
            if word_length:
                words = list(filter(lambda w: len(w) == word_length, words))
        return random.choice(words).replace("\n", "")

    @property
    def chars_to_guess(self):
        """Returns the remaining chars to guess, in the right position"""
        res = []
        for i, c in enumerate(self.word_to_guess):
            if not self.known_chars[i]:
                res.append(c)
            else:
                res.append(False)
        return res

    @property
    def get_partial_word(self):
        return "".join([c or "*" for c in self.known_chars])

    def display_word(self, word):
        res = ""
        for i, c in enumerate(word):
            if i >= len(self.word_to_guess):
                break
            if c == self.word_to_guess[i]:
                res += f"\033[30;42m{c}\033[m"
            elif c in self.chars_in_word:
                res +=  f"\033[30;44m{c}\033[m"
            else:
                res += c
        return res


    def turn(self, guess):
        if self.turn_nr > NUMBER_OF_TRIES:
            return False, False
        chars_in_word = set()
        chars_not_in_word = set()
        if guess == self.word_to_guess:
            return True, self.display_word(guess)
        for i, c in enumerate(guess):
            if i >= len(self.word_to_guess):
                break
            if self.word_to_guess[i] == c:
                self.known_chars[i] = c
            elif c in self.word_to_guess:
                self.chars_in_word.add(c)
            else:
                self.chars_not_in_word.add(c)
        self.turn_nr += 1
        chars_in_word = set()
        for c in self.chars_in_word:
            if c in self.chars_to_guess:
                chars_in_word.add(c)
        self.chars_in_word = chars_in_word
        return False, self.display_word(guess)
